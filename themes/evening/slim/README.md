# "Evening" SLiM login manager theme

This is documentation for configure SLiM login manager in evening-style, like in my awesome config.

##HowTo:
**All steps need root privileges, because SLiM configured system-wide.**
1. Create new SLiM theme:
```sh
# cd /usr/share/slim/themes/
# mkdir evening && cd evening
# cp -R <homedir>/.config/awesome/themes/evening/slim/* .
```

2. Configure SLiM for use new theme. Open file `/etc/slim.conf` and set parameter `current_theme` to `evening`.

3. Enjoy
