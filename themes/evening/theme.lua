-------------------------------
--  "Evening Clouds" awesome theme  --
--    By Andrey Avdey (talkerbox)   --
-------------------------------

-- {{{ Main
local os = os
local home = os.getenv( "HOME" )
local themes_path = home .. "/.config/awesome/themes/evening/"

theme = {}
theme.wallpaper = themes_path .. "bg/geometric.jpg"
-- }}}

-- {{{ Styles
-- If you want use not default system font for awesome panel - change <theme.font> to neccessary font. But this will not work with installed Korean and etc. symbols on system
theme.font      = "FontAwesome 8"
theme.font_bg   = "LiberationSansMono 11"

-- {{{ Colors
theme.fg_normal = "#f8f8f0"
theme.fg_focus  = "#75B5E6"
theme.fg_urgent = "#ff6735"
theme.bg_normal = "#2F3030"
theme.bg_focus  = "#181818"
theme.bg_urgent = "#2F3030"
-- }}}

-- {{{ Borders
theme.border_width  = "1"
theme.border_normal = "#2F3030"
theme.border_focus  = "#474848"
theme.border_marked = "#ff6735"
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = "#2F3030"
theme.titlebar_bg_normal = "#2F3030"
-- }}}

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- Example:
--theme.taglist_bg_focus = "#ff6735"
-- }}}

-- {{{ Widgets
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
theme.fg_widget        = "#f8f8f0"
theme.fg_urgent_widget = "#ff6735"
theme.fg_hot_widget    = "#ff6735"
theme.fg_warm_widget   = "#DDB700"
theme.fg_cold_widget   = "#AEE239"
theme.fg_normal_widget = "#75B5E6"
theme.bg_widget        = "#474848"
-- }}}

-- {{{ Mouse finder
theme.mouse_finder_color = "#ff6735"
-- mouse_finder_[timeout|animate_timeout|radius|factor]
-- }}}

-- {{{ Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = "20"
theme.menu_width  = "150"
-- }}}

-- {{{ Icons
-- {{{ Taglist
-- theme.taglist_squares_sel   = themes_path .. "taglist/squarefz.png"
-- theme.taglist_squares_unsel = themes_path .. "taglist/squarez.png"
-- theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Misc
theme.awesome_icon           = themes_path .. "awesome-icon.png"
-- theme.menu_submenu_icon      = home .. "/.config/awesome/themes/default/submenu.png"
-- theme.tasklist_floating_icon = home .. "/.config/awesome/themes/default/tasklist/floatingw.png"
-- }}}

-- {{{ Layout
theme.layout_tile       = themes_path .. "layouts/tile.png"
theme.layout_tileleft   = themes_path .. "layouts/tileleft.png"
theme.layout_tilebottom = themes_path .. "layouts/tilebottom.png"
theme.layout_tiletop    = themes_path .. "layouts/tiletop.png"
theme.layout_fairv      = themes_path .. "layouts/fairv.png"
theme.layout_fairh      = themes_path .. "layouts/fairh.png"
theme.layout_spiral     = themes_path .. "layouts/spiral.png"
theme.layout_dwindle    = themes_path .. "layouts/dwindle.png"
theme.layout_max        = themes_path .. "layouts/max.png"
theme.layout_fullscreen = themes_path .. "layouts/fullscreen.png"
theme.layout_magnifier  = themes_path .. "layouts/magnifier.png"
theme.layout_floating   = themes_path .. "layouts/floating.png"
-- }}}

-- {{{ Titlebar
theme.titlebar_close_button_focus  = themes_path .. "titlebar/close_focus.png"
theme.titlebar_close_button_normal = themes_path .. "titlebar/close_normal.png"

theme.titlebar_ontop_button_focus_active  = themes_path .. "titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = themes_path .. "titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path .. "titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = themes_path .. "titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = themes_path .. "titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = themes_path .. "titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path .. "titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = themes_path .. "titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = themes_path .. "titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = themes_path .. "titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = themes_path .. "titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = themes_path .. "titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = themes_path .. "titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = themes_path .. "titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path .. "titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = themes_path .. "titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}

return theme
