-- Standard awesome library
local gears = require('gears')
local menubar = require('menubar')
local awful = require('awful')
awful.rules = require('awful.rules')
require('awful.autofocus')
-- Widget and layout library
local wibox = require('wibox')
local vicious = require('vicious')
local tyrannical = require('tyrannical')
-- Theme handling library
local beautiful = require('beautiful')
-- Notification library
local naughty = require('naughty')
-- Load Debian menu entries
require('debian.menu')

local os = os

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title = 'Oops, there were errors during startup!',
        text = awesome.startup_errors
    })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal('debug::error', function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({
            preset = naughty.config.presets.critical,
            title = 'Oops, an error happened!',
            text = err
        })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
home_dir = os.getenv( 'HOME' )
beautiful.init(home_dir ..'/.config/awesome/themes/evening/theme.lua')

-- This is used later as the default applications to run.
terminal = 'terminator'
editor = os.getenv('EDITOR') or 'editor'
editor_cmd = terminal .. ' -e ' .. editor
sys_reboot = 'systemctl reboot'
sys_poweroff = 'systemctl poweroff'
vol_tool = 'pavucontrol'

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = 'Mod4'

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts = {
    awful.layout.suit.max,
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    -- awful.layout.suit.tile.left,
    -- awful.layout.suit.tile.bottom,
    -- awful.layout.suit.tile.top,
    -- awful.layout.suit.fair,
    -- awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.spiral,
    -- awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max.fullscreen,
    -- awful.layout.suit.magnifier
}
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

-- Scr count if you use two screens
scr = math.max(1, screen.count())

-- Naughty visual settings
naughty.config.defaults.timeout          = 3
naughty.config.defaults.margin           = 12
naughty.config.defaults.font             = beautiful.font or "Verdana 8"
naughty.config.defaults.fg               = beautiful.fg_normal_widget or '#75B5E6'
naughty.config.defaults.bg               = beautiful.bg_normal or '#2F3030'
naughty.config.defaults.border_width     = 0

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tyrannical.tags = {
    {
        name            = '  ',
        init            = true,
        exclusive       = true,
        screen          = 1,
        force_screen    = true,
        layout          = layouts[1],
        class           = { 'xterm', 'XTerm', 'terminator', 'gnome-terminal' }
    },
    {
        name            = '  ',
        init            = true,
        exclusive       = true,
        force_screen    = true,
        screen          = 1,
        layout          = layouts[1],
        class           = { 'Sublime_text', 'VIM' }
    },
    {
        name            = '  ',
        init            = true,
        exclusive       = true,
        force_screen    = true,
        screen          = scr,
        layout          = layouts[1],
        class           = { 'Firefox', 'Iceweasel', 'Chrome' }
    },
    {
        name            = '  ',
        init            = true,
        exclusive       = true,
        screen          = scr,
        force_screen    = true,
        layout          = layouts[3],
        class           ={ 'Skype', 'Pidgin', 'ViberPC' }
    },
    {
        name            = '  ',
        init            = true,
        exclusive       = true,
        screen          = scr,
        force_screen    = true,
        layout          = layouts[3],
        class           = { 'Icedove', 'Thunderbird' }
    },
    {
        name            = '  ',
        init            = true,
        exclusive       = false,
        screen          = scr,
        force_screen    = true,
        layout          = layouts[1],
    },
}

-- Ignore the tiled layout for the matching clients
tyrannical.properties.floating = {
    'gimp',
}
tyrannical.properties.maximized = {
    'ViberPC',
}

tyrannical.settings.block_children_focus_stealing   = true --Block popups ()
tyrannical.settings.group_children                  = true --Force popups/dialogs to have the same tags as the parent client
tyrannical.settings.force_odd_as_intrusive          = true

-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu
myawesomemenu = {
    { 'manual', terminal .. ' -e man awesome' },
    { 'edit config', editor_cmd .. ' ' .. awesome.conffile },
    { 'restart', awesome.restart },
    { 'quit', awesome.quit }
}

mymainmenu = awful.menu({
    items = {
        { 'awesome', myawesomemenu, beautiful.awesome_icon },
        { 'Debian', debian.menu.Debian_menu.Debian },
        { 'Reboot', sys_reboot },
        { 'Power off', sys_poweroff }
    }
})

mylauncher = awful.widget.launcher({
    image = beautiful.awesome_icon,
    menu = mymainmenu
})

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}


-- {{{ Actions and messages
local mes_id = 1

actions = {
    ['volume_up']       = 'amixer -q set Master 5%+',
    ['volume_down']     = 'amixer -q set Master 5%-',
    ['volume_mute']     = 'amixer -q set Master toggle',
    ['lock'] = 'slimlock',
    ['set_monitors_1']  = 'xrandr --output HDMI1 --off && xrandr --output LVDS1 --preferred --primary',
    ['set_monitors_2']  = 'xrandr --output HDMI1 --preferred --above LVDS1 --primary && xrandr --output LVDS1 --preferred',
    ['moc_next']        = 'mocp -f',
    ['moc_prev']        = 'mocp -r',
    ['moc_toggle']      = 'mocp -G',
    ['moc_play']        = 'mocp -p',
    ['moc_pause']       = 'mocp -P',
    ['moc_forward']     = 'mocp -k 10',
    ['moc_backward']    = 'mocp -k -10',
    ['bright_up']       = 'sudo brightness up',
    ['bright_down']     = 'sudo brightness down',
}

messages = {
    ['volume_up'] = {
        title       = ' Vol',
        text        = '+ 5%',
        replaces_id = mes_id
    },
    ['volume_down'] = {
        title       = ' Vol',
        text        = '- 5%',
        replaces_id = mes_id
    },
    ['volume_mute'] = {
        title       = ' Vol',
        text        = 'Mute toggle',
        replaces_id = mes_id
    },
    ['moc_toggle'] = {
        title       = ' MOC player',
        text        = 'Pause toggle',
        replaces_id = mes_id + 1
    },
    ['moc_play'] = {
        title       = ' MOC player',
        text        = 'Start play',
        replaces_id = mes_id + 1
    },
    ['moc_pause'] = {
        title       = ' MOC player',
        text        = 'Pause play',
        replaces_id = mes_id + 1
    },
    ['moc_forward'] = {
        title       = ' MOC player',
        text        = 'Forward 10s',
        replaces_id = mes_id + 1
    },
    ['moc_info'] = {
        text            = awful.util.pread('mocp -i'),
        timeout         = 5,
        hover_timeout   = 0.1,
        width           = 400,
        replaces_id     = mes_id + 3
    },
    ['moc_backward'] = {
        title       = ' MOC player',
        text        = 'Backward 10s',
        replaces_id = mes_id + 1
    },
    ['bright_up'] = {
        title       = 'Brightness',
        text        = '+ 300',
        replaces_id = mes_id + 2
    },
    ['bright_down'] = {
        title       = 'Brightness',
        text        = '- 300',
        replaces_id = mes_id + 2
    },
    ['date_info'] = {
        text            = os.date('%A') .. ', '
            .. os.date('%e') .. '.'
            .. os.date('%m') .. '.'
            .. os.date('%Y'),
        timeout         = 3,
        hover_timeout   = 0.1,
        screen          = 1,
        replaces_id     = mes_id + 4
    },
}


function run_action(action, widget)
    if actions[action] then
        awful.util.spawn_with_shell(actions[action])
    end

    if widget then
        vicious.force({widget})
        vicious.force({widget})  -- strange fix, because i've get new value only on next action :)
    end

    if messages[action] then
        naughty.notify(messages[action])
    end
end

-- }}}


-- {{{ Vicious widgets
-- Separator
wgt_separator = wibox.widget.textbox()
wgt_separator:set_text(' ')


-- Moc player
wgt_moc = wibox.widget.textbox()

wgt_moc:buttons(
    awful.util.table.join(
        awful.button({ }, 2, function () run_action('moc_toggle', wgt_moc) end),
        awful.button({ }, 1, function () run_action('moc_prev', wgt_moc) end),
        awful.button({ }, 3, function () run_action('moc_next', wgt_moc) end)
    )
)

wgt_moc:connect_signal('mouse::enter', function () run_action('moc_info') end)

vicious.register(
    wgt_moc,
    vicious.widgets.moc,

    function(widget, args)
        local color         = beautiful.fg_widget
        local color_artist  = beautiful.fg_normal_widget

        if args[1] == 'WAIT' or args[1] == 'STOP' then
            artist, song = ''
        else
            artist  = string.sub(string.gsub(args[2], '"\'', ' '), 1, 16)
            song    = string.sub(string.gsub(args[3], '"\'', ' '), 1, 32 - string.len(artist))

            if string.len(artist) < string.len(args[2]) then
                artist = artist .. '…'
            end

            if string.len(song) < string.len(args[3]) then
                song = song .. '…'
            end

            if string.len(song) == 0 then
                song    = string.sub(string.match(args[4], '.*/(.*)$'), 1, 32)
                artist  = ''
            end
        end


        local values = {
            ['WAIT']    = {'', 'Not running', ''},
            ['STOP']    = {'', 'Stopped', ''},
            ['PLAY']    = {'', artist .. ':', song},
            ['PAUSE']   = {'', artist .. ':', song},
        }

        return '<span background="' .. beautiful.bg_widget
        .. '" font="'               .. beautiful.font_bg
        .. '"> <span color="'       .. color_artist
        .. '" font="'               .. beautiful.font
        .. '">'                     .. values[args[1]][1]
        .. ' '                      .. values[args[1]][2]
        ..'</span> <span color="'   .. color
        .. '" font="'               .. beautiful.font
        .. '">'                     .. values[args[1]][3]
        .. '</span> </span>'

    end, 7
)


-- Thermal
wgt_thermal = wibox.widget.textbox()

vicious.register(
    wgt_thermal,
    vicious.widgets.thermal,

    function(widget, args)
        local color = beautiful.fg_widget

        if args[1] > 59 and args[1] < 80 then
            color = beautiful.fg_warm_widget
        elseif args[1] > 79 then
            color = beautiful.fg_hot_widget
        end

        return '<span background="' .. beautiful.bg_widget
        .. '" font="'               .. beautiful.font_bg
        .. '"> <span color="'       .. color
        .. '" font="'               .. beautiful.font
        .. '"> ' .. args[1]
        .. '°</span> </span>'

    end, 17, 'thermal_zone1'
)


-- CPU usage
wgt_cpu = wibox.widget.textbox()

vicious.register(
    wgt_cpu,
    vicious.widgets.cpu,

    function(widget, args)
        local color = beautiful.fg_widget

        if args[1] > 59 and args[1] < 80 then
            color = beautiful.fg_warm_widget
        elseif args[1] > 79 then
            color = beautiful.fg_hot_widget
        end

        return '<span background="' .. beautiful.bg_normal
        .. '" font="'               .. beautiful.font_bg
        .. '"> <span color="'  .. color
        .. '" font="'               .. beautiful.font
        .. '"> '               .. args[1]
        .. '%</span> </span>'

    end, 19
)


-- Memory usage
wgt_memory = wibox.widget.textbox()

vicious.register(
    wgt_memory,
    vicious.widgets.mem,

    function(widget, args)
        local color = beautiful.fg_widget

        if args[1] > 59 and args[1] < 80 then
            color = beautiful.fg_warm_widget
        elseif args[1] > 79 then
            color = beautiful.fg_hot_widget
        end

        return '<span background="' .. beautiful.bg_widget
        .. '" font="'               .. beautiful.font_bg
        .. '"> <span color="'       .. color
        .. '" font="'               .. beautiful.font
        .. '"> '                   .. args[1]
        .. '%</span> </span>'

    end, 13
)


-- Battary
wgt_battary = wibox.widget.textbox()

vicious.register(
    wgt_battary,
    vicious.widgets.bat,

    function(widget, args)
        local color = beautiful.fg_widget
        local icon  = ''
        local val   = ' ' .. args[2] .. '%'

        if args[1] == '-' then
            val = ' ' .. args[3] .. 'm'
            color = beautiful.fg_warm_widget

            if args[2] < 15 then
                icon = ''
                color = beautiful.fg_hot_widget
            elseif args[2] >= 16 and args[2] < 25 then
                icon = ''
            elseif args[2] >= 26 and args[2] < 50 then
                icon = ''
            elseif args[2] >= 51 and args[2] < 75 then
                icon = ''
            elseif args[2] >= 76 then
                icon = ''
            end
        else
            if args[3] ~= 'N/A' then
                color = beautiful.fg_cold_widget
            else
                val = ''
            end
        end

        return '<span background="' .. beautiful.bg_normal
        .. '" font="'               .. beautiful.font_bg
        .. '"> <span color="'       .. color
        .. '" font="'               .. beautiful.font
        .. '">'                     .. icon
        .. ''                       .. val
        .. '</span> </span>'

    end, 59, 'BAT0'
)


-- Volume indicator
wgt_volume = wibox.widget.textbox()

wgt_volume:buttons(
    awful.util.table.join(
        awful.button({ }, 1, function () awful.util.spawn(vol_tool, true) end),
        awful.button( { }, 3, function () run_action('volume_mute', wgt_volume) end),
        awful.button({ }, 4, function () run_action('volume_up', wgt_volume) end),
        awful.button({ }, 5, function () run_action('volume_down', wgt_volume) end)
    )
)

vicious.register(wgt_volume, vicious.widgets.volume,

    function(widget, args)
        local values = {
            ['♫']   = { beautiful.fg_widget, ''},
            ['♩']   = { beautiful.fg_urgent_widget, ' '},
        }

        return '<span background="' .. beautiful.bg_widget
        .. '" font="'               .. beautiful.font_bg
        .. '"> <span color="'       .. values[args[2]][1]
        .. '" font="'               .. beautiful.font
        .. '">'                     .. values[args[2]][2]
        .. ' '                      .. args[1]
        .. '%</span> </span>'

    end, 3600, 'Master'
)


-- Date and time
wgt_datetime = wibox.widget.textbox()

wgt_datetime:connect_signal('mouse::enter', function () run_action('date_info') end)

vicious.register(
    wgt_datetime,
    vicious.widgets.date,

    '<span background="'    .. beautiful.bg_normal
    .. '" font="'           .. beautiful.font_bg
    .. '"> <span color="'   .. beautiful.fg_widget
    .. '" font="'           .. beautiful.font
    .. '">%R</span> </span>',

    60
)


-- }}}
-- {{{ Wibox

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
    awful.button({ }, 1, awful.tag.viewonly),
    awful.button({ modkey }, 1, awful.client.movetotag),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, awful.client.toggletag),
    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
)
mytasklist = {}
mytasklist.buttons = awful.util.table.join(

    awful.button({ }, 1, function (c)
        if c == client.focus then
            c.minimized = true
        else
            -- Without this, the following
            -- :isvisible() makes no sense
            c.minimized = false
            if not c:isvisible() then
                awful.tag.viewonly(c:tags()[1])
            end
            -- This will also un-minimize
            -- the client, if needed
            client.focus = c
            c:raise()
        end
    end),

    awful.button({ }, 3, function ()
        if instance then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({
                theme = { width = 250 }
            })
        end
    end),

    awful.button({ }, 4, function ()
        awful.client.focus.byidx(1)
        if client.focus then client.focus:raise() end
    end),

    awful.button({ }, 5, function ()
        awful.client.focus.byidx(-1)
        if client.focus then client.focus:raise() end
    end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(
        awful.util.table.join(
           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)
        )
    )
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = 'top', screen = s, height = 18 })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    -- left_layout:add(mylauncher)
    left_layout:add(mylayoutbox[s])
    left_layout:add(mytaglist[s])
    left_layout:add(mypromptbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()

    if s == 1 then
        right_layout:add(wgt_moc)
        right_layout:add(wgt_separator)
        right_layout:add(wibox.widget.systray())
        right_layout:add(wgt_separator)
        right_layout:add(wgt_thermal)
        right_layout:add(wgt_cpu)
        right_layout:add(wgt_memory)
        right_layout:add(wgt_battary)
        right_layout:add(wgt_volume)
    end

    right_layout:add(wgt_datetime)

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,}, 'Left',   awful.tag.viewprev),
    awful.key({ modkey,}, 'Right',  awful.tag.viewnext),
    awful.key({ modkey,}, 'Escape', awful.tag.history.restore),

    awful.key({ modkey,}, 'j',
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),

    awful.key({ modkey,}, 'k',
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),

    awful.key({ modkey,}, 'w', function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, 'Shift'}, 'j', function () awful.client.swap.byidx(  1) end),
    awful.key({ modkey, 'Shift'}, 'k', function () awful.client.swap.byidx( -1) end),
    awful.key({ modkey, 'Control' }, 'j', function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, 'Control' }, 'k', function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, 'u', awful.client.urgent.jumpto),
    awful.key({ modkey,           }, 'Tab',
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, 'Return', function () awful.util.spawn(terminal) end),
    awful.key({ modkey, 'Control' }, 'r', awesome.restart),
    awful.key({ modkey, 'Shift'   }, 'q', awesome.quit),

    awful.key({ modkey,           }, 'l',     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, 'h',     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, 'Shift'   }, 'h',     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, 'Shift'   }, 'l',     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, 'Control' }, 'h',     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, 'Control' }, 'l',     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, 'space', function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, 'Shift'   }, 'space', function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, 'Control' }, 'n', awful.client.restore),


    -- {{{ User keys

    -- Volume control
    awful.key({ }, '#122',  function () run_action('volume_down', wgt_volume) end),

    awful.key({ }, '#123',  function () run_action('volume_up', wgt_volume) end),

    awful.key({ }, '#121',  function () run_action('volume_mute', wgt_volume) end),

    -- Brightness control
    awful.key({ }, '#232',  function () run_action('bright_down') end),
    awful.key({ }, '#233',  function () run_action('bright_up') end),

    -- Lock screen
    awful.key({ modkey }, 'F1', function () run_action('moc_pause') run_action('lock') end),

    -- Change monitors layout
    awful.key({ modkey }, 'F9', function () run_action('set_monitors_2') end),
    awful.key({ modkey }, 'F10', function () run_action('set_monitors_1') end),

    -- MOC player control
    awful.key({ modkey }, '#59', function () run_action('moc_prev', wgt_moc) end),
    awful.key({ modkey }, '#60', function () run_action('moc_next', wgt_moc) end),
    awful.key({ modkey }, '#61', function () run_action('moc_toggle', wgt_moc) end),
    awful.key({ modkey, 'Control' }, '#59', function () run_action('moc_backward', wgt_moc) end),
    awful.key({ modkey, 'Control' }, '#60', function () run_action('moc_forward', wgt_moc) end),
    awful.key({ modkey, 'Control' }, '#61', function () run_action('moc_play', wgt_moc) end),


    -- Prompt
    awful.key({ modkey },            'r',     function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, 'x',
              function ()
                  awful.prompt.run({ prompt = 'Run Lua code: ' },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir('cache') .. '/history_eval')
              end),
    -- Menubar
    awful.key({ modkey }, 'p', function() menubar.show() end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, 'f',      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, 'Shift'   }, 'c',      function (c) c:kill()                         end),
    awful.key({ modkey, 'Control' }, 'space',  awful.client.floating.toggle                     ),
    awful.key({ modkey, 'Control' }, 'Return', function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, 'o',      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, 't',      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, 'n',
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, 'm',
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

function get_screen_and_tag(i)
    local screen = mouse.screen

    local num_tags_first_scr = table.getn(awful.tag.gettags(1))

    if i > num_tags_first_scr then
        screen = 2
        tag = i - num_tags_first_scr
    else
        screen = 1
        tag = i
    end

    return screen, tag

end

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, '#' .. i + 9,
            function ()
                screen, a = get_screen_and_tag(i)

                local tag = awful.tag.gettags(screen)[a]
                if tag then
                    awful.screen.focus(screen)
                    awful.tag.viewonly(tag)
                end
            end
        ),
        -- Toggle tag.
        awful.key({ modkey, 'Control' }, '#' .. i + 9,
            function ()
                screen, a = get_screen_and_tag(i)

                local tag = awful.tag.gettags(screen)[a]
                if tag then
                     awful.tag.viewtoggle(tag)
                end
            end
        ),
        -- Move client to tag.
        awful.key({ modkey, 'Shift' }, '#' .. i + 9,
            function ()
                if client.focus then
                    screen, a = get_screen_and_tag(i)

                    local tag = awful.tag.gettags(screen)[a]

                    if tag then
                        awful.client.movetotag(tag)
                    end
                end
            end
        ),
        -- Toggle tag.
        awful.key({ modkey, 'Control', 'Shift' }, '#' .. i + 9,
            function ()
                if client.focus then

                    screen, a = get_screen_and_tag(i)

                    local tag = awful.tag.gettags(screen)[a]

                    if tag then
                        awful.client.toggletag(tag)
                    end
                end
            end
        )
    )
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the 'manage' signal).
awful.rules.rules = {
    {
    -- All clients will match this rule.
        rule = { },
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
        },
    },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal('manage', function (c, startup)
    -- Enable sloppy focus
    -- c:connect_signal('mouse::enter', function(c)
    --     if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
    --         and awful.client.focus.filter(c) then
    --         client.focus = c
    --     end
    -- end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    elseif not c.size_hints.user_position and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count change
        awful.placement.no_offscreen(c)
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == 'normal' or c.type == 'dialog') then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
            awful.button({ }, 1, function()
                client.focus = c
                c:raise()
                awful.mouse.client.move(c)
            end),
            awful.button({ }, 3, function()
                client.focus = c
                c:raise()
                awful.mouse.client.resize(c)
            end)
        )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:buttons(buttons)

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        local title = awful.titlebar.widget.titlewidget(c)
        title:set_align('center')
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c):set_widget(layout)
    end
end)

client.connect_signal('focus', function(c) c.border_color = beautiful.border_focus end)
client.connect_signal('unfocus', function(c) c.border_color = beautiful.border_normal end)
-- }}}


function re_run(prg)
    awful.util.spawn_with_shell('killall ' .. prg )
    awful.util.spawn_with_shell( prg .. ' &')
end

re_run('xxkb')
-- }}}
--
