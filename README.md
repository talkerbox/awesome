# Talkerbox awesome-wm config

This is my awesome config, that I use everyday on work and home.

## Works on:
```sh
$ awesome -v
awesome v3.5.6 (For Those About To Rock)
 • Build: Jun  5 2015 06:28:50 for x86_64 by gcc version 4.9.2 (root@minjo)
 • Compiled against Lua 5.1.5 (running with Lua 5.1)
 • D-Bus support: ✔
```

## Plugins:
- **tyrannical** (used by git submodule way - some later about this ) - for tag organization
- **vicious** (already installed in awesome-extra, moc-player widget only added by me) - for widgets
- **beautiful** - for themes
- **naughty** - for notifications
- **gears, menubar, awful, wibox** - like in standart awesome config

## Additionals:
- theme for awesome - *evening*
- theme for SLiM (Simple Login Manager) - evening-style for SLiM login manager
- font FontAwesome for icons


## How to install:
**Before this, I recommend backup your previos config!**

1. Clone this repo in your awesome config directory (like `~/.configs/awesome`).
2. Update git submodules (tyrannical) by run:
```sh
$ cd ~/.config/awesome && git submodules update --init
```
3. Install FontAwesome.otf on your system. On debian - just copy file in `~/.fonts/` directory
4. Restart your awesome-wm and enjoy

For configure SLiM - read this [documentation](themes/evening/slim/README.md).
